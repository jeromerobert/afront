#include "PointCloudGuidanceField.h"
#include <fstream>

PointCloudGuidanceField::PointCloudGuidanceField(real_type _rho, real_type _reduction)
    :NoMeshGuidanceField(_rho, _reduction), kdtree(NULL) {
}

void PointCloudGuidanceField::readMetricFile(char * fileName) {
    std::ifstream in(fileName, std::ios::in | std::ios::binary);
    if(!in.is_open()) {
        std::cout << "Error: Cannot open " << fileName << std::endl;
    }
    in.seekg (0, in.end);
    int pointNumber = in.tellg() / sizeof(double) / 4;
    in.seekg (0, in.beg);
    double buffer[4];
    points.resize(pointNumber);
    Box3 bb;
    for(int i = 0; i < pointNumber; i++) {
        in.read((char*)buffer, sizeof(buffer));
        points[i].point.reset(buffer[0], buffer[1], buffer[2]);
        points[i].size = buffer[3];
        bb.update(points[i].point);
    }
    kdtree = new gtb::KDTree<int, real_type, PointCloudGuidanceField>(10, bb, *this);
    for(int i = 0; i < pointNumber; i++) {
        kdtree->Insert(i, false);
    }
    kdtree->MakeTree();
}

real_type PointCloudGuidanceField::targetSize(const Point3 &p) {
    return points[kdtree->FindMin(p)].size;
}
