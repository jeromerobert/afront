/*
Copyright 2017 Jerome Robert


This file is part of Afront.

Afront is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Afront is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA
 */

#include "NoMeshGuidanceField.h"

struct Context {
    bool done;
    const Point3 & point;
    Context(const Point3 & p): done(false), point(p){}
};

NoMeshGuidanceField::NoMeshGuidanceField(real_type _rho, real_type _reduction):
    GuidanceField(_rho, 0, std::numeric_limits<double>::max(), _reduction)
{
}

void* NoMeshGuidanceField::OrderedPointTraverseStart(const Point3 &p)
{
    return new Context(p);
}

bool NoMeshGuidanceField::OrderedPointTraverseNext(
    void *ctx, real_type &squared_dist, Point3 &, real_type &ideal)
{
    Context * context = static_cast<Context*>(ctx);
    if(context->done)
		return false;
    context->done = true;
    ideal = targetSize(context->point);
    squared_dist = ideal * ideal;
	return true;
}

void NoMeshGuidanceField::OrderedPointTraverseEnd(void *ctx)
{
    delete static_cast<Context*>(ctx);
}
