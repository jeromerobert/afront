#include "common.h"
#include "parallel.h"
#include "output_controller_vtp.h"

void OutputControllerVTP::AddVertex(int, const Point3 &p, const Vector3 &, bool) {
  size_t n = points_.size();
  points_.resize(n+1);
  points_[n][0] = static_cast<float>(p.x());
  points_[n][1] = static_cast<float>(p.y());
  points_[n][2] = static_cast<float>(p.z());
}

void OutputControllerVTP::AddTriangle(int, int a, int b, int c) {
  size_t n = triangles_.size();
  triangles_.resize(n+1);
  triangles_[n][0] = a;
  triangles_[n][1] = b;
  triangles_[n][2] = c;
}

void OutputControllerVTP::Finish() {
  std::ofstream out(filename_, std::ios::binary);
  out << "<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">";
  out << "<PolyData>";
  out << "<Piece NumberOfPoints=\"" << points_.size() <<
      "\" NumberOfPolys=\"" << triangles_.size() <<"\">";

  out << "<Points><DataArray type=\"Float32\" NumberOfComponents=\"3\" "
      << "format=\"appended\" offset=\"0\"/></Points>";
  size_t offset=sizeof(int) + points_.size() * sizeof(float) * 3;

  //polys
  out << "<Polys><DataArray type=\"Int32\" Name=\"connectivity\""
      << " format=\"appended\" offset=\"" << offset << "\"/>";
  offset += sizeof(int) + triangles_.size() * sizeof(int) * 3;

  out << "<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\""
      << " offset=\"" << offset << "\"/></Polys>";
  offset+=sizeof(int) * (triangles_.size()+1);

  out << "</Piece></PolyData>";
  out << "<AppendedData encoding=\"raw\"> _";
  unsigned size = sizeof(float) * points_.size() * 3;
  out.write(reinterpret_cast<const char*>(&size), sizeof(size));
  for(auto it = points_.begin(); it != points_.end(); ++it)
    out.write(reinterpret_cast<const char*>(*it), sizeof(float[3]));

  size = sizeof(int) * triangles_.size() * 3;
  out.write(reinterpret_cast<const char*>(&size), sizeof(size));
  for(auto it = triangles_.begin(); it != triangles_.end(); ++it)
    out.write(reinterpret_cast<const char*>(*it), sizeof(int[3]));

  size = sizeof(int) * triangles_.size();
  out.write(reinterpret_cast<const char*>(&size), sizeof(size));
  for(int i = 3; i <= 3*triangles_.size(); i+=3)
    out.write(reinterpret_cast<const char*>(&i), sizeof(i));
}
