#pragma once
/*
Copyright 2017 Jerome Robert


This file is part of Afront.

Afront is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Afront is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA
*/

#include "NoMeshGuidanceField.h"
#include <vector>

class PointCloudGuidanceField: public NoMeshGuidanceField {
    struct KDTreePoints {
        Point3 point;
        real_type size;
    };
    gtb::KDTree<int, real_type, PointCloudGuidanceField> * kdtree;
    /** {x, y, z, targetSize, x, y, z, ... } */
    std::vector<KDTreePoints> points;
public:
    PointCloudGuidanceField(real_type _rho, real_type _reduction);
    void readMetricFile(char * fileName);
    virtual ~PointCloudGuidanceField() {}
    virtual real_type targetSize(const Point3 &p);
    const Point3& operator()(unsigned idx) const {
        return points[idx].point;
    }
};
