/*
Copyright 2012 Jerome Robert


This file is part of Afront.

Afront is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Afront is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA
 */

#include "OutputControllerBin.h"
#include "gtb/graphics/point3.hpp"
OutputControllerBin::OutputControllerBin(const char *filename)
{
	if(filename)
	{
		fout = fopen(filename, "ab");
		if(fout == NULL)
			perror("Cannot open output file");
		//ensure that the output file will contain a valid node
		//number if things goes wrong
		int32_t zero = 0;
		fwrite(&zero, 1, sizeof(zero), fout);
		fclose(fout);
		fout = fopen(filename, "r+b");
		fseek(fout, -4, SEEK_END);
		fflush(fout);
	}
}

void OutputControllerBin::AddVertex(int index, const Point3 &p, const Vector3 &n, bool boundary)
{
	if(!boundary)
		points.push_back(new Point3(p));
}

void OutputControllerBin::Finish() {
	int32_t size = points.size() * 3;
	fwrite(&size, 1, sizeof(size), fout);
	double buf[3];
	const Point3 * p;
	for(int i = 0; i < points.size(); i++)
	{
		p = points[i];
		buf[0] = p->x();
		buf[1] = p->y();
		buf[2] = p->z();
		fwrite(&buf, 1, sizeof(buf), fout);
		delete p;
	}
	fclose(fout);
	if (child)
		child->Finish();
	points.clear();
}