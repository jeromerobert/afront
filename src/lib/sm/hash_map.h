#if __GNUC__ >= 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >=3)
	#include <stdlib.h>
	#include <tr1/unordered_map>
	#define hash_map std::tr1::unordered_map
#elif _MSC_DEV
#include <hash_map>
using std::hash_map;
#else
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#endif
