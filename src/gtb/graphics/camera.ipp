
/*

Copyright 2007 University of Utah


This file is part of Afront.

Afront is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Afront is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA

*/


#include <gtb/math/math.hpp>


GTB_BEGIN_NAMESPACE


template<class T>
inline const typename tCamera<T>::Point3 &tCamera<T>::origin() const
{
    return _cs.origin();
}

template<class T>
inline const typename tCamera<T>::Point3 &tCamera<T>::get_origin() const
{
    return _cs.origin();
}


template<class T>
inline const typename tCamera<T>::Vector3 &tCamera<T>::backwards() const
{
    return _cs.z();
}


template<class T>
inline typename tCamera<T>::Vector3 tCamera<T>::towards() const
{
    return -_cs.z();
}


template<class T>
inline const typename tCamera<T>::Vector3 &tCamera<T>::up() const
{
    return _cs.y();
}


template<class T>
inline const typename tCamera<T>::Vector3 &tCamera<T>::get_up() const
{
    return _cs.y();
}


template<class T>
inline typename tCamera<T>::Vector3 tCamera<T>::down() const
{
    return -_cs.y();
}


template<class T>
inline const typename tCamera<T>::Vector3 &tCamera<T>::right() const
{
    return _cs.x();
}


template<class T>
inline const typename tCamera<T>::Vector3 &tCamera<T>::get_right() const
{
    return _cs.x();
}


template<class T>
inline typename tCamera<T>::Vector3 tCamera<T>::left() const
{
    return -_cs.x();
}


template<class T>
inline typename tCamera<T>::value_type tCamera<T>::x_fov() const
{
    return _x_fov;
}


template<class T>
inline typename tCamera<T>::value_type tCamera<T>::y_fov() const
{
    return _y_fov;
}


template<class T>
inline typename tCamera<T>::value_type tCamera<T>::aspect() const
{
    // value_type w = 2.0 * _near_distance * tan(_x_fov);
    // value_type h = 2.0 * _near_distance * tan(_y_fov);
    // return w / h;
    return tan(_x_fov) / tan(_y_fov);
}


template<class T>
inline typename tCamera<T>::value_type tCamera<T>::near_distance() const
{
    return _near_distance;
}


template<class T>
inline typename tCamera<T>::value_type tCamera<T>::far_distance() const
{
    return _far_distance;
}


template<class T>
inline const tCoordinateSystem<T> &tCamera<T>::coordinate_system() const
{
    return _cs;
}


template<class T>
inline typename tCamera<T>::Matrix4 tCamera<T>::matrix() const
{
    return _cs.matrix();
}


template<class T>
inline typename tCamera<T>::Matrix4 tCamera<T>::inverse_matrix() const
{
    return _cs.inverse_matrix();
}


template<class T>
inline bool tCamera<T>::sees(const Point3 &p) const
{
    for (unsigned i = 0; i < 6; i++) {
        if (_planes[i].signed_distance(p) <= 0.0) {
            return false;
        }
    }
    return true;
}


template<class T>
inline void tCamera<T>::pitch(value_type angle)
{
    rotate(tLine3<T>(origin(), right()), angle);
}


template<class T>
inline void tCamera<T>::yaw(value_type angle)
{
    rotate(tLine3<T>(origin(), up()), angle);
}


template<class T>
inline void tCamera<T>::roll(value_type angle)
{
    rotate(tLine3<T>(origin(), towards()), angle);
}


GTB_END_NAMESPACE
